# Turnkey Pedal 

Turnkey Pedal is an Arduino sketch for using an ESP32 micro-controller as
a bluetooth page turner (e.g. for use with [forScore](https://forscore.co)).
Paired with a [TinyPICO](https://www.tinypico.com/), a cheap
[pedal switch](https://www.amazon.com/gp/product/B0838T6MNR/), and a standard
[li-po battery](https://www.amazon.com/gp/product/B08FD39Y5R/), I made a fully
self-contained bluetooth pedal for about $45.

## About the Code

This project uses [PlatformIO](https://docs.platformio.org/en/latest/what-is-platformio.html)
as a development framework, which enables it to work nicely with modern IDEs
like CLion or Visual Studio Code. It runs on Arduino/C++, with the arduino 
“sketch” in `src/main.cpp`. 

You will need to 
[install PlatformIO Core](https://docs.platformio.org/en/latest/core/installation.html) 
to build the code or flash your own TinyPICO. Don't worry, it’s easy — and you
can install the IDE tools for Visual Studio or CLion — which make development
even easier.

The TinyPICO comes flashed from the factory with 
[MicroPython](https://micropython.org/), but unfortunately I could not find a
BLE HID library for MicroPython that would work with Apple products, so here 
we are. 😔

While no MicroPython implementations fit my needs, the 
[ESP32 BLE Keyboard](https://github.com/T-vK/ESP32-BLE-Keyboard) library for
Arduino worked for me as advertised, and it is used here.

### installing dependencies

```
> platformio lib install
```

### building a binary

```
> platformio run
Processing tinypico (platform: espressif32; board: tinypico; framework: arduino)
--------------------------------------------------------------------------------
Verbose mode can be enabled via `-v, --verbose` option
CONFIGURATION: https://docs.platformio.org/page/boards/espressif32/tinypico.html
PLATFORM: Espressif 32 (3.5.0) > Unexpected Maker TinyPICO
HARDWARE: ESP32 240MHz, 320KB RAM, 4MB Flash
DEBUG: Current (esp-prog) External (esp-prog, iot-bus-jtag, jlink, minimodule, olimex-arm-usb-ocd, ... )
PACKAGES:
 - framework-arduinoespressif32 3.10006.210326 (1.0.6)
 - tool-esptoolpy 1.30100.210531 (3.1.0)
 - toolchain-xtensa32 2.50200.97 (5.2.0)
LDF: Library Dependency Finder -> https://bit.ly/configure-pio-ldf
LDF Modes: Finder ~ deep+, Compatibility ~ soft
Found 30 compatible libraries
Scanning dependencies...
Dependency Graph
|-- <ESP32 BLE Keyboard> 0.3.2
|   |-- <ESP32 BLE Arduino> 1.0.1
|-- <TinyPICO Helper Library> 1.4.0
|   |-- <SPI> 1.0
|-- <SPI> 1.0
Building in release mode
Linking .pio/build/tinypico/firmware.elf
Retrieving maximum program size .pio/build/tinypico/firmware.elf
Checking size .pio/build/tinypico/firmware.elf
Advanced Memory Usage is available via "PlatformIO Home > Project Inspect"
RAM:   [=         ]   9.5% (used 31132 bytes from 327680 bytes)
Flash: [========  ]  82.7% (used 1083798 bytes from 1310720 bytes)
====================== [SUCCESS] Took 4.66 seconds ============================
```

### flashing a TinyPICO device

```
> platformio run --target upload -e tinypico
...
Writing at 0x00010000... (2 %)
Writing at 0x0001b7f4... (5 %)
...
Writing at 0x0010d2e9... (97 %)
Writing at 0x00112d9b... (100 %)
Wrote 1084016 bytes (604734 compressed) at 0x00010000 in 53.4 seconds (effective 162.4 kbit/s)...
Hash of data verified.

Leaving...
Hard resetting via RTS pin...
============================= [SUCCESS] Took 61.50 seconds =====================
```

## Using the Pedal

* Tap once to wake the pedal from deep sleep
* While the LED flashes blue, pair with your device (after 30s, it will sleep)
* When paired, the LED will stop flashing and glow a lighter shade of blue
* Tap for less than 350ms (about 1/3 of a second) to advance the page (keyboard right-arrow)
* Tap and hold for 350ms to 5s to go back a page (keyboard left arrow)
* Tap and hold for greater than 5s to power off (deep sleep)
* Tap and hold for greater than 20s to reset the controller
* Plug in to USB to charge the battery


## Background

There are some great sheet music apps for tablets now (such as
[forScore](https://forscore.co)) which work readily with any number of
commercial bluetooth pedals for activating page turns with a tap of the foot.

The commercial pedals recommended by forScore are all upwards of $80 — a bit
pricey for the musician on a budget — so I decided to see what I could put
together on a shoestring. 

I quickly learned that these bluetooth pedals simply appear to iOS as bluetooth
HID keyboards, sending a right-arrow key to advance to the next page, and
a left arrow key to go back a page. It doesn’t get much simpler than that! 

## Design

Taking inspiration from [$6 DIY bluetooth sheet music page turn pedal with
ESP32](https://codeandlife.com/2021/05/23/diy-bluetooth-sheet-music-page-turn-pedal-with-esp32/),
I set out to build a fully self-contained bluetooth pedal that presents
a BLE HID keyboard and emits a right-arrow on short press, and a left arrow
on long press. 

My final rig cost more than $6 for a few reasons because: 

* I wasn’t willing to wait over a month for a pedal or board to be shipped 
  from China (and faster shipping makes it roughly the same cost as Amazon)
* I used a smaller and more expensive ESP32 board
* The original $6 didn't seem to include any kind of power source

I found the following components that all fit nicely together:

* [TinyPICO](https://www.tinypico.com/) ESP32 microcontroller (it really is
  tiny!) — $20
* [Fielect FS-1 Foot Pedal
  Switch](https://www.amazon.com/gp/product/B0838T6MNR/) — $10
* [EEMB 3.7V 1100mAh Lipo Battery](https://www.amazon.com/gp/product/B08FD39Y5R/) — $13

The pedal is very simple, consisting of a top and bottom half, held together by
screws about three-quarters of the way up the side of the housing. The bottom
half of the pedal contains a relay with a lever attached to it. When the pedal
is depressed, the lever triggers the relay, switching the circuit from one pole 
of the relay to the other. Below you can see the pedal's original wiring, 
looking from the top down at the bottom half of the pedal:

<img src="img/pedal-original-wiring.jpeg" height="512" alt="pedal with original wiring" />

After de-soldering and removing the cable that comes attached to the pedal,
there is plenty of room for the battery and the TinyPICO in the top half of the
housing, allowing the USB port of the TinyPICO to align with the housing’s
cable cut-out. 

I wired Pin 4 of the TinyPICO to the bottom (NC) pole of the relay, and wired
the common pole of the relay to the TinyPICO's ground pin. The pin is then 
configured as an `INPUT_PULLUP`, which reads `HIGH` when the switch is pressed
and `LOW` when it's not. There are various other ways you could wire this and
make it work, but you may need to update the accompanying code accordingly.

Below you can see it all wired up (as you can see, I accidentally
swapped the wire colors while soldering — I meant to wire green to ground
— I have since labeled the wires internally to avoid future confusion).

<img src="img/wired-pedal.jpeg" width="512" alt="rewired pedal" />

You will also notice the thread impressions left by the screw in the
Sugru that I used to glue everything together. I later cut both screws to
a shorter length to avoid contact with the battery (they come longer
than necessary).

## Outcome

Overall this project turned out well. The pedal is completely usable and works
as intended with my iPad (and for testing my Macbook Pro, too). The relay used
in the pedal is a bit loud (clicky like a mechanical keyboard switch) so it
may not be suitable for recordings or performances. It works great for practicing 
and rehearsal, but I may need to eventually spend the $80 on a solid-state design.


