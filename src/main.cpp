#include <Arduino.h>
#include <BleKeyboard.h>
#include <TinyPICO.h>

// arguments set name, manufacturer, and battery level, respectively
BleKeyboard pedalboard("turnkey-pedal", "wryfi", 100);

// Pin that is wired to our pedal terminal
#define BUTTON_PIN 4

TinyPICO tp = TinyPICO();

void setup() {
    // enable the board to wake up from deep sleep when the button is pushed
    esp_sleep_enable_ext0_wakeup(GPIO_NUM_4, 1);

    // configure pin; important to PULLUP (activate internal resistor)
    pinMode(BUTTON_PIN, INPUT_PULLUP);

    // start the serial console for debugging
    Serial.begin(115200);
    Serial.println("\nHello from TinyPICO on Turnkey Pedal!");
    Serial.println("-------------------------------------\n");

    // get and display battery voltage
    float voltage = tp.GetBatteryVoltage();
    char voltstr[5];
    dtostrf(voltage, 4, 2, voltstr);
    char voltmsg[40];
    sprintf(voltmsg, "Battery voltage: %sv", voltstr);
    Serial.println(voltmsg);

    // display a message if battery is charging
    bool charging = tp.IsChargingBattery();
    if (charging) {
        Serial.println("Battery is charging");
    }

    // turn on power to the LED and clear its configuration
    tp.DotStar_SetPower(true);
    tp.DotStar_Clear();

    // start the keyboard service
    Serial.println("Starting BLE keyboard ... \n");
    pedalboard.begin();
}

void loop() {
    if (pedalboard.isConnected()) {

        // set led to blue-green at quarter-brightness
        tp.DotStar_SetBrightness(63);
        tp.DotStar_SetPixelColor(0, 200, 255);

        // if our button is pushed, count how long in ms it is pushed
        unsigned long timer = 0;
        while (digitalRead(BUTTON_PIN) == HIGH) {
            delay(1);
            timer++;
        }

        // do various things based on the timer length ...
        // between 1 and 350 ms, send KEY_RIGHT_ARROW
        // between 350 and 5000 ms, send KEY_LEFT_ARROW
        // between 5000 and 20000 ms, enter deep sleep
        // over 20000, reset device
        if (timer > 1 && timer < 350) {
            Serial.println("short button press");
            pedalboard.write(KEY_RIGHT_ARROW);
            delay(20);
            pedalboard.releaseAll();
        } else if (timer > 350 && timer < 5000) {
            Serial.println("long button press");
            pedalboard.write(KEY_LEFT_ARROW);
            delay(20);
            pedalboard.releaseAll();
        } else if (timer > 5000 && timer < 20000) {
            Serial.println("really long press");
            tp.DotStar_SetPixelColor(255, 0, 0);
            tp.DotStar_SetBrightness(255);
            delay(1000);
            Serial.println("sleeping ...");
            tp.DotStar_SetPower(false);
            esp_deep_sleep_start();
        } else if (timer > 20000) {
            Serial.println("reset activated");
            tp.DotStar_SetPixelColor(255, 0, 0);
            tp.DotStar_SetBrightness(255);
            delay(500);
            ESP.restart();
        }
    } else {    // bluetooth is not connected
        // blink the LED and wait 30 seconds for a bluetooth connection
        unsigned int attempts = 30;
        while (attempts > 0 && !pedalboard.isConnected()) {
            tp.DotStar_SetPixelColor(0, 0, 255);
            tp.DotStar_SetBrightness(255);
            Serial.println("bluetooth connecting ...");
            delay(500);
            tp.DotStar_Clear();
            delay(500);
            attempts--;
        }

        // if still not connected, then enter deep sleep
        if (!pedalboard.isConnected()) {
            tp.DotStar_SetPixelColor(255, 0, 0);
            tp.DotStar_SetBrightness(255);
            delay(500);
            tp.DotStar_SetPower(false);
            esp_deep_sleep_start();
        }
    }

    // run main loop on 20 ms delay
    delay(20);
}